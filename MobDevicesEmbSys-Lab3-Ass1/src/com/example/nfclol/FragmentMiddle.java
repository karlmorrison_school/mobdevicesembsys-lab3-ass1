package com.example.nfclol;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragmentMiddle extends Fragment {

	protected TextView tvId;
	private ArrayList<TextView> textViewList = new ArrayList<TextView>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_middle, container, false);
		tvId = (TextView) view.findViewById(R.id.tv_id);
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


	}

	protected void clearValues() {
		tvId.setText("");
		
		LinearLayout techView = (LinearLayout) getActivity().findViewById(R.id.tech_list);	
		techView.removeAllViews();
		
	}

	protected void addTech(String[] techlist) {
		for (String tech : techlist){
			// Skriv ut �tech� i ditt gr�nssnitt 
			TextView tv = new TextView(getActivity());
			tv.setText(tech);
			
			//textViewList.add(tv);
			
			LinearLayout techView = (LinearLayout) getActivity().findViewById(R.id.tech_list);
			techView.addView(tv);
		} 
	}

}
