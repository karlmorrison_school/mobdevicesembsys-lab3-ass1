package com.example.nfclol;

import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;

public class MainActivity extends Activity {

	private NfcManager mNfcManager;
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	private FragmentMiddle fragMiddle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		fragMiddle = new FragmentMiddle();
		getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragMiddle).commit();


		mNfcManager = (NfcManager) this.getSystemService(Context.NFC_SERVICE);
		mNfcAdapter = mNfcManager.getDefaultAdapter();

		if (mNfcAdapter == null) 
			return;

		Intent intent = new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		mPendingIntent = PendingIntent.getActivity(this, 0,intent, 0);

	}


	@Override 
	protected void onNewIntent(Intent intent) {
		resolveIntent(intent);
		super.onNewIntent(intent);
	}

	private void resolveIntent(Intent intent) { 
		String action = intent.getAction(); 

		if (action.equals(NfcAdapter.ACTION_TAG_DISCOVERED) 
				|| action.equals(NfcAdapter.ACTION_TECH_DISCOVERED) 
				|| action.equals(NfcAdapter.ACTION_NDEF_DISCOVERED)) {
			fragMiddle.clearValues(); // H�mta NFC taggen

			Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG); // H�mta dess id (UID, hexadecimal) 
			byte[] id = tag.getId(); 
			if (id != null) {		
				String idString = "";
				
				for (int i=0, len=id.length; i<len; i++) {
					idString+=id[i];
				}

				fragMiddle.tvId.setText(idString);
			} else { 
				// Inget id... } // H�mta all teknologi som st�ds i taggen 
			}
			fragMiddle.addTech(tag.getTechList()); // Skriv ut �tech� i ditt gr�nssnitt 
		}
	}

	@Override
	public void onPause() {
		if (mNfcAdapter != null) { 
			mNfcAdapter.disableForegroundDispatch(this); 
		}
		super.onPause(); 
	}

	@Override
	protected void onResume() {
		if (mNfcAdapter != null) { 
			mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
		}
		super.onResume();
	}
}
